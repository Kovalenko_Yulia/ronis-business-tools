$(document).ready(function(){
    $('.slick-slider').slick({
        infitite: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 5000,
        fade: true,
        cssEase: 'ease-in-out',
        pauseOnFocus: false,
        pauseOnHover: true,
        arrows: true
    });

    $('#fullpage').fullpage({
        navigation: true,
        navigationPosition: 'right',
        showActiveTooltip: false,
        slidesNavigation: false,
        slidesNavPosition: 'bottom',
        loopTop: true,
        loopBottom: true,
    });

    //change slider's number & item name after slide
    $('.slides-number span.total').html($('.slick-slider .slides').length);

    $('.slick-slider').on('afterChange', function(){
        var itemName = $(this).find('.slick-track').find('.slides.slick-active').find('img').attr('alt');
        var itemNumber = $(this).find('.slick-track').find('.slides.slick-active').index();
        itemNumber = +itemNumber + 1;

        $('.section.page-5 span.name').html(itemName);
        $('.section.page-5 .slides-number .current').html(itemNumber);
    });

});